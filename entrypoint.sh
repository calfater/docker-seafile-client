#!/usr/bin/env bash
echo "--------------------------------------------------------------------------------"
echo "User  : $UNAME ($UID)"
echo "Group : $GNAME ($GID)"

echo -e "$UNAME:x:${UID}:${GID}:$UNAME:/home/$UNAME:/bin/bash\n" >> /etc/passwd
echo -e "$GNAME:x:${GID}:$GNAME\n" >> /etc/group

chown -R $UNAME\: /home/$UNAME/config /home/$UNAME/*.sh

sudo -i -u $UNAME bash -c "/home/$UNAME/script.sh $@"
