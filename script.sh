#!/usr/bin/env bash

function line() {
  echo "--------------------------------------------------------------------------------"
}

function s() {
    seaf-cli $@ -c "$CONFIG"
}

function ask_for() {
  echo -n "$1" 1>&2
  if [[ "" == "$2" ]]; then
    read v
  else
    read -s v
    echo -n "" 1>&2
  fi
  echo $v
}

set -m

trap "echo Press 'q' to quit" SIGINT

export -f s
export CONFIG=$HOME/config/cli
export DATA=$HOME/config/data
export LIBRARIES=$HOME/libraries

line
echo "CONFIG=$CONFIG"
echo "DATA=$DATA"
echo "LIBRARIES=$LIBRARIES"

mkdir -p "$DATA"

if [ ! -d "$CONFIG" ]; then
	s init -d "$DATA"
fi

line
s start

line
s list

if [[ "config" == $1 ]]; then
  while [[ 1 ]]; do

    line
    echo "a) Add"
    echo "r) Remove"
    echo "s) Status"
    echo "b) start Bash"
    echo "q) Quit"

    read -s -n1 choice

    if [[ "a" == $choice ]]; then
      line
      server=$(ask_for "Server url : ")
      username=$(ask_for "Username (email) : ")
      password=$(ask_for "Password : " 1)

      echo "Please wait while receiving libraries list"
      s list-remote -s "$server" -u "$username" -p "$password"

      if [[ 0 -eq $? ]]; then
        library=$(ask_for "Remote library id : ")
        folder="$LIBRARIES/$(ask_for "Local folder name : ")"

        echo "$username@$server/$library => $folder"
        mkdir -p "$folder"
        s sync -l "$library" -s "$server" -u "$username" -p "$password" -d "$folder"
      else
        echo "Login error"
      fi

    elif [[ "r" == $choice ]]; then
      line
      s list
      line
	  path=$(ask_for "Path to remove (the Path, not the name) : ")
	  s desync -d "$path"

    elif [[ "s" == $choice ]]; then
      line
      s status
      line
      s list

    elif [[ "b" == $choice ]]; then
      line
      echo "-> seaf-cli aliased as s"
      echo "-> Use ^D to leave bash"
      bash

    elif [[ "q" == $choice ]]; then
      exit;

    else
	  echo "invalid choice"

    fi
    echo
  done
elif [[ "shell" == $1 ]]; then
    bash
else
    while [[ 1 ]]; do
      sleep 60;
      line;
      date
      s status
      s list
    done
fi

s stop

